package com.starting11.android.core;

import java.util.ArrayList;

import android.content.Context;

import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.RootContext;
import com.googlecode.androidannotations.annotations.sharedpreferences.Pref;
import com.starting11.android.utils.Prefs_;

/**
 * @author Hein Win Toe
 * @version 1.0
 */
@EBean
public abstract class BaseAdapter<E> extends android.widget.BaseAdapter {

	protected ArrayList<E> objects;

	@Pref
	protected Prefs_ pref;

	@RootContext
	protected Context mContext;

	public BaseAdapter(){

	}
	
	public void setArraylist(ArrayList<E> objects){
		this.objects = objects; 
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return objects.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return objects.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	protected Context getContext(){
		return this.mContext;
	}
}
