package com.starting11.android.core;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.sharedpreferences.Pref;
import com.starting11.android.utils.Prefs_;
import com.starting11.android.utils.Utils;

/**
 * Base Activity class
 *
 * @author Hein Win Toe (hein[at]nexlabs[dot]co)
 * @since 1.0.0
 */

@EActivity
public abstract class BaseActivity extends SherlockActivity {

	@Pref
	protected Prefs_ pref;

	@Bean
	protected Utils utils;

	protected Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if (getSupportActionBar() != null){

			getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#89b63c")));

			int titleId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");    
			if ( 0 == titleId ) titleId = com.actionbarsherlock.R.id.abs__action_bar_title;

			TextView actionBarTitle = (TextView)findViewById(titleId);
			actionBarTitle.setTextColor(Color.parseColor("#FFFFFF"));
			
			actionBarTitle.setTypeface(utils.getTypefaceMagnetoBold());
		}

		mContext = getBaseContext();
	}
}
