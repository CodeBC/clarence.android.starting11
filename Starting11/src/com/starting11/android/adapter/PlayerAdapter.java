package com.starting11.android.adapter;

import android.view.View;
import android.view.ViewGroup;

import com.googlecode.androidannotations.annotations.EBean;
import com.starting11.android.core.BaseAdapter;
import com.starting11.android.object.Match;
import com.starting11.android.view.PlayetItemView;
import com.starting11.android.view.PlayetItemView_;

@EBean
public class PlayerAdapter extends BaseAdapter<Match> {

	public PlayerAdapter() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		PlayetItemView view;
		if (convertView == null) {
			view = PlayetItemView_.build(mContext);
		} else {
			view = (PlayetItemView) convertView;
		}

		Match match = (Match) getItem(0);
		
		view.bind(match.team1_players.get(position),match.team2_players.get(position),match.team1_players.get(position).isSeperator);

		return view;
	}
	
}
