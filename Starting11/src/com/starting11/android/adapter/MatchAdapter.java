package com.starting11.android.adapter;

import java.util.ArrayList;

import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;

import com.googlecode.androidannotations.annotations.EBean;
import com.starting11.android.core.BaseAdapter;
import com.starting11.android.object.Match;
import com.starting11.android.view.MatchItemHeaderView;
import com.starting11.android.view.MatchItemHeaderView_;
import com.starting11.android.view.MatchItemView;
import com.starting11.android.view.MatchItemView_;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersSimpleAdapter;

@EBean
public class MatchAdapter extends BaseAdapter<Match> implements StickyGridHeadersSimpleAdapter, SectionIndexer {

	private int[] mSectionIndices;
	private Character[] mSectionLetters;

	public MatchAdapter() {

	}

	public void init(){
		mSectionIndices = getSectionIndices();
		mSectionLetters = getSectionLetters();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		MatchItemView view;

		if (convertView == null) {
			view = MatchItemView_.build(mContext);
		} else {
			view = (MatchItemView) convertView;
		}

		view.bind((Match) getItem(position));

		return view;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		MatchItemHeaderView view;

		if (convertView == null) {
			view = MatchItemHeaderView_.build(mContext);
		} else {
			view = (MatchItemHeaderView) convertView;
		}

		view.bind(((Match) getItem(position)).type);

		return view;
	}

	private int[] getSectionIndices() {
		ArrayList<Integer> sectionIndices = new ArrayList<Integer>();
		char lastFirstChar = (objects.get(0).type + "").charAt(0);
		sectionIndices.add(0);
		for (int i = 1; i < objects.size(); i++) {
			if ((objects.get(i).type + "").charAt(0)!= lastFirstChar) {
				lastFirstChar = (objects.get(i).type + "").charAt(0);
				sectionIndices.add(i);
			}
		}
		int[] sections = new int[sectionIndices.size()];
		for (int i = 0; i < sectionIndices.size(); i++) {
			sections[i] = sectionIndices.get(i);
		}
		return sections;
	}

	private Character[] getSectionLetters() {
		Character[] letters = new Character[mSectionIndices.length];
		for (int i = 0; i < mSectionIndices.length; i++) {
			letters[i] = objects.get(mSectionIndices[i]).type.charAt(0);
		}
		return letters;
	}

	/**
	 * Remember that these have to be static, postion=1 should always return
	 * the same Id that is.
	 */
	@Override
	public long getHeaderId(int position) {
		// return the first character of the country as ID because this is what
		// headers are based upon
		return objects.get(position).type.hashCode();
	}

	@Override
	public int getPositionForSection(int section) {
		if (section >= mSectionIndices.length) {
			section = mSectionIndices.length - 1;
		} else if (section < 0) {
			section = 0;
		}
		return mSectionIndices[section];
	}

	@Override
	public int getSectionForPosition(int position) {
		for (int i = 0; i < mSectionIndices.length; i++) {
			if (position < mSectionIndices[i]) {
				return i - 1;
			}
		}
		return mSectionIndices.length - 1;
	}

	@Override
	public Object[] getSections() {
		return mSectionLetters;
	}

}
