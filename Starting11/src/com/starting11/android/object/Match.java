package com.starting11.android.object;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class Match implements Serializable{

	//List - start
	@SerializedName("match_id")
	public int match_id;

	@SerializedName("match_start_time")
	public String start_time;

	@SerializedName("team1")
	public String team_1;

	@SerializedName("team2")
	public String team_2;

	@SerializedName("team1_score")
	public int team_1_score;
	
	@SerializedName("team1_penaltyshoot")
	public int team1_penaltyshoot;
	
	@SerializedName("team2_penaltyshoot")
	public int team2_penaltyshoot;
	
	public int team_1_hfs;

	@SerializedName("team2_score")
	public int team_2_score;
	
	public int team_2_hfs;

	@SerializedName("played_minutes")
	public String played_minute;

	@SerializedName("played_date")
	public String played_date;

	@SerializedName("country")
	public String country;

	@SerializedName("lineup_ready")
	public String lineup_ready = "";
	
	public String type;
	
	public String currentTimeStatus;
	
	//List - End
	
	//Detail - Start
	@SerializedName("station")
	public String station;
	
	@SerializedName("ref")
	public String ref;
	
	@SerializedName("isHalfTime")
	public String isHalfTime;
	
	@SerializedName("current_time")
	public String current_time;
	//Detail - End
	
	public ArrayList<Player> team1_scorers = new ArrayList<Player>();
	
	public ArrayList<Player> team2_scorers = new ArrayList<Player>();
	
	public ArrayList<Player> team1_players = new ArrayList<Player>();
	
	public ArrayList<Player> team2_players = new ArrayList<Player>();
	
	public ArrayList<Player> team1_miss_penalty = new ArrayList<Player>();
	
	public ArrayList<Player> team2_miss_penalty = new ArrayList<Player>();
}
