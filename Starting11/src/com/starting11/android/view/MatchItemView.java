package com.starting11.android.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;
import com.starting11.android.object.Match;

@EViewGroup(R.layout.listitem_match)
public class MatchItemView extends LinearLayout{

	@ViewById
	TextView time;

	@ViewById
	TextView team1;
	
	@ViewById
	TextView score;
	
	@ViewById
	TextView team2;
	
	@ViewById
	TextView line_up;
	
	SimpleDateFormat old_df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	SimpleDateFormat past_df = new SimpleDateFormat("yyyy-MM-dd");
	
	SimpleDateFormat new_df = new SimpleDateFormat("dd-MMM");
	
	SimpleDateFormat current_df = new SimpleDateFormat("dd-MMM HH:mm");

	public MatchItemView(Context context) {
		super(context);
	}

	public void bind(Match obj) {

		old_df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		past_df.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		new_df.setTimeZone(TimeZone.getDefault());
		current_df.setTimeZone(TimeZone.getDefault());
		
		team1.setText(obj.team_1);
		team2.setText(obj.team_2);
		
		if(obj.lineup_ready.equalsIgnoreCase("N")){
			line_up.setText("Line-Up");
			line_up.setVisibility(TextView.INVISIBLE);
			//line_up.setBackgroundColor(Color.parseColor("#333333"));
		}else{
			line_up.setText("Line-Up");
			line_up.setVisibility(TextView.VISIBLE);
			//line_up.setBackgroundColor(Color.parseColor("#8BC945"));
		}
		
		if(obj.type.equalsIgnoreCase("current")){
			
			/*if(obj.isHalfTime.equalsIgnoreCase("Y"))
				time.setText("HT");
			else if(obj.isHalfTime.equalsIgnoreCase("N"))
				time.setText(obj.played_minute);*/
			
			if(obj.currentTimeStatus.equalsIgnoreCase("")){
				time.setText(obj.played_minute);
			}else{
				time.setText(obj.currentTimeStatus);
			}
			
			score.setText(obj.team_1_score + "   " + obj.team_2_score);
		}else if(obj.type.equalsIgnoreCase("upcoming")){
			
			try {
				Date d = old_df.parse(obj.start_time);
				obj.start_time = current_df.format(d);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			time.setText(obj.start_time);
			score.setText(" vs ");
		
		}else if(obj.type.equalsIgnoreCase("past")){
			
			try {
				Date d = past_df.parse(obj.played_date);
				obj.played_date = new_df.format(d);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			time.setText(obj.played_date);
			score.setText(obj.team_1_score + "   " + obj.team_2_score);
		}

	}

}
