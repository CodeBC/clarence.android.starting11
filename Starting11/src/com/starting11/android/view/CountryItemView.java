package com.starting11.android.view;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;
import com.starting11.android.R;
import com.starting11.android.object.Country;

@EViewGroup(R.layout.listitem_country)
public class CountryItemView extends RelativeLayout {

	@ViewById
	TextView title;

	@ViewById
	ImageView icon;

	public CountryItemView(Context context) {
		super(context);
	}

	public void bind(Country obj) {
		title.setText(obj.name);

		icon.setImageResource(obj.icon);
		
		setBackgroundColor(obj.bg);

	}
}