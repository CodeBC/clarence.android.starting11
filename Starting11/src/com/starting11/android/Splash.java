package com.starting11.android;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Fullscreen;
import com.googlecode.androidannotations.annotations.NoTitle;
import com.googlecode.androidannotations.annotations.res.StringRes;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.starting11.android.core.BaseActivity;
import com.starting11.android.exception.ReturnException;
import com.starting11.android.ui.CountrySelect_;
import com.starting11.android.utils.L;

@EActivity(R.layout.splash)
@NoTitle
@Fullscreen
public class Splash extends BaseActivity {

	@StringRes(R.string.url_keygen)
	String url;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		if(!BuildConfig.DEBUG) Crashlytics.start(this);
	}

	@AfterViews
	public void calledAfterViewInjection() {

		if(pref.key().exists()){
			
			L.i(pref.key().get());
			
			Thread splashTread = new Thread() {
				@Override
				public void run() {
					try {
						Thread.sleep(3000);
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						finish();

						Intent main = new Intent(Splash.this, CountrySelect_.class);
						startActivity(main);
					}
				}
			};

			splashTread.start();
		}else{
			if(Starting11App.hasInternet(mContext)){
				Ion
				.with(mContext)
				.load(url)
				.setBodyParameter("client_platform", "A")
				.setBodyParameter("device_token", utils.udid())
				.asString()
				.setCallback(new FutureCallback<String>() {	
					@Override
					public void onCompleted(Exception e, String result) {

						try {
							if (e != null) throw e;

							JsonParser parser = new JsonParser();

							JsonObject jobj_main = (JsonObject) parser.parse(result.trim());

							int status = jobj_main.get("status").getAsInt();

							if(status == 1){
								String key = jobj_main.get("key").getAsString();
								pref.key().put(key);

								finish();

								Intent main = new Intent(Splash.this, CountrySelect_.class);
								startActivity(main);

							}else{
								throw new ReturnException("Server Return is " + result);
							}
						}
						catch (Exception ex) {
							ex.printStackTrace();

							Toast.makeText(mContext, "Unable to connect", Toast.LENGTH_SHORT).show();
						}

					}

				});
			}else{

				L.i("No Connection");

				Toast.makeText(mContext, "No Active Internet Connection", Toast.LENGTH_SHORT).show();
			}
		}
	}

}
