package com.starting11.android.utils;

import com.googlecode.androidannotations.annotations.sharedpreferences.DefaultString;
import com.googlecode.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * Shared Preference Controller
 *
 * @author Hein Win Toe (hein[at]nexlabs[dot]co)
 * @since 1.0.0
 */

@SharedPref(value=SharedPref.Scope.UNIQUE)
public interface Prefs {
    
	@DefaultString("")
    String key();
	
}
