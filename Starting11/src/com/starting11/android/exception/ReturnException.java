package com.starting11.android.exception;

public class ReturnException extends Exception {

	public ReturnException() {
		// TODO Auto-generated constructor stub
	}

	public ReturnException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public ReturnException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public ReturnException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
